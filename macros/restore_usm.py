"""This module contains macros that demonstrate the usage of macro parameters"""

from sardana.macroserver.macro import *

from datetime import datetime
from PyTango import *
import time
import traceback

class restore_usm(Macro):
	"""the sequencer restore USM mode for BPM"""
		
	def run(self):
		try:
			self.output("[restore_usm] Start macro at " + str(datetime.now()))				
			#
                	# import devices
	                #
			self.output("[restore_usm] Import Devices")
			dp_all = DeviceProxy ('srdiag/bpm/all')
			dp_sparksall = DeviceProxy ('srdiag/bpm/all-sparks')
			dp_brilsall = DeviceProxy ('srdiag/bpm/all-brilliances')
			dp_clock = DeviceProxy ('srdiag/bpm/aclock')
			dp_TBTsynchro = DeviceProxy ('srdiag/bpm/tbtsynchro')
			self.output("[restore_usm] Import Devices OK")

			self.output("[restore_usm] restore config on Sparks All")		
			# on the Sparks :
			dp_sparksall.write_attribute("ADC_MaskOffset", 0)
			dp_sparksall.write_attribute("ADC_MaskWindow", 304)
			dp_sparksall.write_attribute("OffsetTune", 300)
			dp_sparksall.write_attribute("CompensateTune", True)
			dp_sparksall.write_attribute("SA_Enable", True)

			self.output("[restore_usm] restore config on Brilliances All")				
			# on the Brilliances :
			dp_brilsall.write_attribute("DSCMode", 2) # note : this will put Switches automatically to Auto Switch
			# 0 is OFF, 1=unity, 2=Mode Auto  , only use 0 or 2
			#dp_brilsall.write_attribute("Switches", 15) is DIRECT
			#dp_brilsall.write_attribute("Switches", 16) is Auto Switch
			dp_brilsall.write_attribute("OffsetTune", 300)
			dp_brilsall.write_attribute("CompensateTune", True)

			self.output("[restore_usm] restore config on librera All")
			# on Sparks and Brilliances :
			dp_all.write_attribute("ADC_Enable", False)
			#dp_all.write_attribute("ADC_Enable", True)
			dp_all.write_attribute("TBT_Enable", False)
			#dp_all.write_attribute("TBT_Enable", True)
			dp_all.write_attribute("TBT_Dec_Enable", True)
			#dp_all.write_attribute("TBT_Dec_Enable", False)
			dp_all.write_attribute("ADC_BufSize", 200)
			dp_all.write_attribute("TBT_BufSize", 500)
		
			self.output("[restore_usm] Succesfull macro at " + str(datetime.now()))
			
		except Exception as e:
			self.output("[restore_usm] macro Failed at " + str(datetime.now()))
			self.output(traceback.format_exc())			
		return

	def on_abort(self):
                """Hook executed when an abort occurs. Overwrite as necessary"""

                self.output("[restore_usm] Abort macro")
                pass

        def on_pause(self):
                """Hook executed when an pause occurs. Overwrite as necessary"""

                self.output ("[restore_usm] Macro is in pause mode, waiting for resume")
                pass
