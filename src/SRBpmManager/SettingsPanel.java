package SRBpmManager;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.IBooleanScalar;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.IEnumScalar;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.attribute.BooleanScalarCheckBoxViewer;
import fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.attribute.BooleanScalarComboEditor;
import fr.esrf.tangoatk.widget.attribute.EnumScalarComboEditor;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;


class MyNumberScalarEditor extends NumberScalarWheelEditor {


  public MyNumberScalarEditor() {
    setAlarmEnabled(false);
    setBackground(UIManager.getColor("Panel.background"));
  }
  
  public void stateChange(AttributeStateEvent evt) {
    if(evt.getState()=="ALARM")
     setBorder(SettingsPanel.redBorder);
    else
     setBorder(null);
  }
  
}

class MyBooleanScalarEditor extends BooleanScalarComboEditor {
  
  public MyBooleanScalarEditor() {
    setBackground(UIManager.getColor("Panel.background"));
  }
  
  public void stateChange(AttributeStateEvent evt) {
    if(evt.getState()=="ALARM")
     setBorder(SettingsPanel.redBorder);
    else
     setBorder(null);
  }
  
}

class MyEnumScalarEditor extends EnumScalarComboEditor {
  
  public MyEnumScalarEditor() {
    setBackground(UIManager.getColor("Panel.background"));
  }
  
  public void stateChange(AttributeStateEvent evt) {
    if(evt.getState()=="ALARM")
     setBorder(SettingsPanel.redBorder);
    else
     setBorder(null);
  }
  
}

class MyVoidVoidCommandEditor extends JButton implements ActionListener {
  
  VoidVoidCommand model;
  
  public MyVoidVoidCommandEditor() {
    super("Execute");
    addActionListener(this);
  }
  
  public void setModel(VoidVoidCommand cmd) {
    model = cmd;    
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if(model!=null) {
      model.execute();
    }    
  }
    
}

class MyVoidVoidCommandEditorWithConfirm extends JButton implements ActionListener {
  
  VoidVoidCommand model;
  
  public MyVoidVoidCommandEditorWithConfirm() {
    super("Execute");
    addActionListener(this);
  }
  
  public void setModel(VoidVoidCommand cmd) {
    model = cmd;    
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if(model!=null) {
      int ok = JOptionPane.showConfirmDialog(this,  "Do you want to execute " + model.getNameSansDevice() + " ?", "Confirmation" , JOptionPane.YES_NO_OPTION);
      if( ok == JOptionPane.YES_OPTION )
        model.execute();
    }
  }
    
}

public class SettingsPanel extends javax.swing.JPanel {
  
  final static Border redBorder = BorderFactory.createLineBorder(Color.RED);
  final static Font numberFont = new Font("Dialog",Font.PLAIN,13);
  private AttributeList attList;
  private CommandList cmdList;
  MainPanel mp;

  public SettingsPanel(MainPanel mp,String title,String[] nameList) {
    
    this.mp = mp;
    
    GridBagConstraints gbc = new GridBagConstraints();
    
    initComponents();
    
    setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),title));
    
    attList = new AttributeList();
    attList.addErrorListener(mp.errWin);
    attList.addErrorListener(ErrorPopup.getInstance());
    cmdList = new CommandList();
    cmdList.addErrorListener(mp.errWin);
    cmdList.addErrorListener(ErrorPopup.getInstance());
    
    gbc.fill = GridBagConstraints.BOTH;
    gbc.gridy = 0;
    
    for(int i=0;i<nameList.length;i++) {

      IEntity att = null;

      if(!nameList[i].startsWith("Cmd")) {
        
      try {
        
        // Attribute 
        
        if(nameList[i].indexOf('/')!=-1) {
          att = attList.add(nameList[i]);          
        } else {        
          att = attList.add(mp.BPMALL_DEVNAME+"/"+nameList[i]);
        }
        
        if( att instanceof INumberScalar ) {
          
          JLabel label = new JLabel(getCanonicalName(nameList[i]));
          label.setFont(ATKConstant.labelFont);
          gbc.insets.top = 0;
          gbc.insets.bottom = 0;
          gbc.insets.right = 5;
          gbc.ipady = 0;
          gbc.gridx = 0;
          add(label,gbc);
          gbc.insets.right = 2;
          gbc.gridx = 1;
          
          if(((INumberScalar) att).isWritable()) {
            MyNumberScalarEditor nse = new MyNumberScalarEditor();
            nse.setFont(numberFont);
            nse.setModel((INumberScalar)att);
            add(nse,gbc);
          } else {
            gbc.ipady = 10;
            SimpleScalarViewer ssv = new SimpleScalarViewer();
            ssv.setModel((INumberScalar) att);
            ssv.setBackgroundColor(getBackground());
            add(ssv,gbc);
          }
          
          
          gbc.gridy++;
                  
          
        } else if ( att instanceof IBooleanScalar ) {
          
          JLabel label = new JLabel(getCanonicalName(nameList[i]));
          label.setFont(ATKConstant.labelFont);
          gbc.insets.top = 2;
          gbc.insets.bottom = 2;
          gbc.ipady = 0;
          gbc.gridx = 0;
          gbc.insets.right = 5;
          add(label,gbc);
          
          if(((IBooleanScalar) att).isWritable()) {
            MyBooleanScalarEditor bse = new MyBooleanScalarEditor();
            bse.setAttModel((IBooleanScalar)att);
            gbc.gridx = 1;
            gbc.insets.right = 2;
            add(bse,gbc);
          } else {
            BooleanScalarCheckBoxViewer bsv = new BooleanScalarCheckBoxViewer();
            bsv.setAttModel((IBooleanScalar)att);
            gbc.gridx = 1;
            gbc.insets.right = 2;
            add(bsv,gbc);
          }
          
          gbc.gridy++;
          
        } else if ( att instanceof IEnumScalar ) {
          
          JLabel label = new JLabel(getCanonicalName(nameList[i]));
          label.setFont(ATKConstant.labelFont);
          gbc.insets.top = 2;
          gbc.insets.bottom = 2;
          gbc.insets.right = 5;
          gbc.ipady = 0;
          gbc.gridx = 0;
          add(label,gbc);
          
          MyEnumScalarEditor ese = new MyEnumScalarEditor();
          ese.setEnumModel((IEnumScalar)att);
          gbc.gridx = 1;
          gbc.insets.top = 2;
          gbc.insets.bottom = 2;
          gbc.insets.right = 2;
          add(ese,gbc);
          
          gbc.gridy++;
          
        }
                
      } catch( ConnectionException e ) {
        
      }
      
      }
      
      // Command
      // Command with confirmation
      if( nameList[i].startsWith("CmdValidation") ) {
        
        String cmdName = nameList[i].substring(13);
        
        try {
          
          att = cmdList.add(mp.BPMALL_DEVNAME+"/"+cmdName);

          if ( att instanceof VoidVoidCommand ) {
            
            JLabel label = new JLabel(cmdName);
            label.setFont(ATKConstant.labelFont);
            gbc.insets.top = 2;
            gbc.insets.bottom = 2;
            gbc.insets.right = 5;
            gbc.ipady = 0;
            gbc.gridx = 0;
            add(label,gbc);
          
            MyVoidVoidCommandEditorWithConfirm vvc = new MyVoidVoidCommandEditorWithConfirm();
            vvc.setModel((VoidVoidCommand)att);
            gbc.gridx = 1;
            gbc.insets.top = 2;
            gbc.insets.bottom = 2;
            gbc.insets.right = 2;
            add(vvc,gbc);
          
            gbc.gridy++;
            
          }
          
        } catch( ConnectionException e ) {         
        }
        
      } else if( nameList[i].startsWith("Cmd") ) {
        
        String cmdName = nameList[i].substring(3);
        
        try {
          
          if( cmdName.indexOf("/")==-1) {          
            att = cmdList.add(mp.BPMALL_DEVNAME+"/"+cmdName);
          } else {
            att = cmdList.add(cmdName);
          }

          if ( att instanceof VoidVoidCommand ) {
            
            JLabel label = new JLabel(getCanonicalName(cmdName));
            label.setFont(ATKConstant.labelFont);
            gbc.insets.top = 2;
            gbc.insets.bottom = 2;
            gbc.insets.right = 5;
            gbc.ipady = 0;
            gbc.gridx = 0;
            add(label,gbc);
          
            MyVoidVoidCommandEditor vvc = new MyVoidVoidCommandEditor();
            vvc.setModel((VoidVoidCommand)att);
            gbc.gridx = 1;
            gbc.insets.top = 2;
            gbc.insets.bottom = 2;
            gbc.insets.right = 2;
            add(vvc,gbc);
          
            gbc.gridy++;
            
          }
          
        } catch( ConnectionException e ) {         
        }
        
      }
      
    }
    gbc.insets.top = 0;
    gbc.insets.bottom = 0;
    gbc.gridwidth = 2;
    gbc.weighty = 1.0;
    add(new JPanel(),gbc);
    
    attList.startRefresher();
            
  }
  
  private String getCanonicalName(String attName) {
    
    if(attName.indexOf('/')==-1) {
      return attName;
    } else {
      int idx = attName.indexOf('-');
      if(idx==-1) {
        return attName;
      } else {
        String s = attName.substring(idx+1);
        return s.substring(0, 1).toUpperCase() + s.substring(1);
      }
    }
    
  }
  
  public void clearModel() {
    
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    setLayout(new java.awt.GridBagLayout());
  }// </editor-fold>//GEN-END:initComponents


  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
}
