/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRBpmManager;

import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.ISpectrumListener;
import fr.esrf.tangoatk.core.NumberSpectrumEvent;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLAxis;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;
import java.awt.Color;
import javax.swing.JOptionPane;

/**
 *
 * @author pons
 */
public class SpectrumPanel extends javax.swing.JPanel implements ISpectrumListener,IJLChartListener {

  JLDataView[] views;
  INumberSpectrum[] models;
  MainPanel mp;

  /**
   * Creates new SpectrumPanel
   */
  public SpectrumPanel(MainPanel mp,String name,String[] panelCfg) {
    
    this.mp = mp;
    
    if(panelCfg.length<3) {
      mp.splash.setVisible(false);
      JOptionPane.showMessageDialog(null, "Wrong SpectrumPanel configuration: " + name, "Error", JOptionPane.ERROR_MESSAGE);
      System.exit(0);            
    }
    
    initComponents();
    int nbView = panelCfg.length - 2;    
    views = new JLDataView[nbView];
    models = new INumberSpectrum[nbView];

    chart.getY1Axis().setAutoScale(true);
    chart.getY1Axis().setName(panelCfg[0]);
    chart.getY2Axis().setAutoScale(true);
    chart.getY2Axis().setName(panelCfg[1]);
    chart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
    chart.setJLChartListener(this);
    
    for(int i=0;i<nbView;i++) {
      views[i] = parseView(panelCfg[i+2]);
      try {
        models[i] = (INumberSpectrum)mp.attList.add(mp.BPMALL_DEVNAME+"/"+views[i].getName());
        models[i].addSpectrumListener(this);
        models[i].refresh();
      } catch( ConnectionException e) {        
      }
    }
    
    String[] cellNames = new String[MainPanel.NBCELL];
    double[] cellPos = new double[MainPanel.NBCELL];
    for(int i=0;i<cellNames.length;i++) {
      cellNames[i] = new String("C" + Integer.toString((i+3)%mp.NBCELL+1));
      cellPos[i] = (double)i*mp.NBBPM;
    }
    
    chart.getXAxis().setLabels(cellNames, cellPos);
    chart.getXAxis().setGridVisible(true);
    chart.getXAxis().setAutoScale(true);

  }
  
  private JLDataView parseView(String def) {
    
    String[] defs = def.split(",");
    if(defs.length<5) {
      JOptionPane.showMessageDialog(null, "Wrong SpectrumPanel configuration (at lest 5 field expected): " + def, "Error", JOptionPane.ERROR_MESSAGE);
      System.exit(0);      
    }
    
    JLDataView ret = new JLDataView();
    ret.setName(defs[0]);
    
    if(defs[1].equalsIgnoreCase("Y1")) {
      chart.getY1Axis().addDataView(ret);
    } else if(defs[1].equalsIgnoreCase("Y2")) {
      chart.getY2Axis().addDataView(ret);
    } else {
      JOptionPane.showMessageDialog(null, "Wrong SpectrumPanel configuration (2nd field, Y1 or Y2 expected): " + def, "Error", JOptionPane.ERROR_MESSAGE);
      System.exit(0);            
    }
    
    int r=0,g=0,b=0;
    
    try {
      r = Integer.parseInt(defs[2]);
      g = Integer.parseInt(defs[3]);
      b = Integer.parseInt(defs[4]);
    } catch(NumberFormatException e) {
      JOptionPane.showMessageDialog(null, "Wrong SpectrumPanel configuration (3rd 4th and 5th field r,g,b number expected): " + def + "\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      System.exit(0);                  
    }
    
    ret.setColor(new Color(r,g,b));
    ret.setMarker(JLDataView.MARKER_DOT);
    ret.setMarkerColor(new Color(r,g,b).darker());
    
    if( defs.length==6 ) {
      // Gain
      try {
        double gain = Double.parseDouble(defs[5]);
        ret.setA1(gain);
      } catch(NumberFormatException e) {
        JOptionPane.showMessageDialog(null, "Wrong SpectrumPanel configuration (6th field gain number expected): " + def + "\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        System.exit(0);                  
      }
      
    }
    
    return ret;

  }
    
  public void clearModel() {
    
    for(int i=0;i<models.length;i++) {
      if(models[i]!=null) {
        models[i].removeSpectrumListener(this);
        models[i] = null;
      }
    }
    
  }
  
  @Override
  public String[] clickOnChart(JLChartEvent jlce) {
    
    String[] ret = new String[2];
    int idx = jlce.getDataViewIndex();    
    ret[0] = "C" + Integer.toString((idx/mp.NBBPM+3)%mp.NBCELL+1) + "-" + mp.BPMIDX[idx%mp.NBBPM] + " [" + mp.BPMTYPE[idx%mp.NBBPM] + "]";
    ret[1] = jlce.getDataView().getName() + "=" + Double.toString(jlce.getTransformedYValue());
    return ret;
    
  }

  public void spectrumChange(NumberSpectrumEvent nse) {

    double[] ret = new double[mp.TOTALBPM];
    for(int i=0;i<mp.TOTALBPM;i++) ret[i] = Double.NaN;
    
    boolean found = false;
    int i = 0;
    while(!found) {
      found = nse.getSource()==models[i];
      if(!found) i++;
    }
    
    if(found) {
      
      double[] values = nse.getValue();
      for(int j=0;j<values.length;j++)
        ret[j] = values[j];
      views[i].reset();
      for(int j=0;j<mp.TOTALBPM;j++) 
        views[i].add(j,ret[j]);
      chart.repaint();
      
    }  
    
  }

  @Override
  public void stateChange(AttributeStateEvent ase) {
  }

  @Override
  public void errorChange(ErrorEvent ee) {
    
    boolean found = false;
    int i = 0;
    while(!found) {
      found = ee.getSource()==models[i];
      if(!found) i++;
    }
    
    if(found) {
      
      views[i].reset();
      for(int j=0;j<mp.TOTALBPM;j++) 
        views[i].add(j,Double.NaN);
      chart.repaint();
      
    }  

    
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    chart = new fr.esrf.tangoatk.widget.util.chart.JLChart();

    setLayout(new java.awt.BorderLayout());
    add(chart, java.awt.BorderLayout.CENTER);
  }// </editor-fold>//GEN-END:initComponents


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private fr.esrf.tangoatk.widget.util.chart.JLChart chart;
  // End of variables declaration//GEN-END:variables
}
