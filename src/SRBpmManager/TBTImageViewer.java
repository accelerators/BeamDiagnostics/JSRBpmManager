/*
 *  Copyright (C) :	2002,2003,2004,2005,2006,2007,2008,2009
 *			European Synchrotron Radiation Facility
 *			BP 220, Grenoble 38043
 *			FRANCE
 *
 *  This file is part of Tango.
 *
 *  Tango is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Tango is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */

package SRBpmManager;

import fr.esrf.tangoatk.widget.util.*;
import fr.esrf.tangoatk.widget.util.chart.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/**
 * Zoom info
 */

class ZoomInfo {
  String text;
  int ratio; // Negative ration means 1/-ratio
  ZoomInfo(String txt,int r) {
    text = txt;
    ratio = r;
  }
};

/**
 * A class to monitor an image using colormap for intensity.
 */
public class TBTImageViewer extends JComponent implements ActionListener, MouseListener,
    J3DTrendListener, IJLChartListener {

  /**
   * Possible zoom values.
   */
  public static final ZoomInfo[] zoomInfos = new ZoomInfo[] {
      new ZoomInfo("10%",-10),
      new ZoomInfo("25%",-4),
      new ZoomInfo("50%",-2),
      new ZoomInfo("100%",1),
      new ZoomInfo("200%",2),
      new ZoomInfo("300%",3),
      new ZoomInfo("400%",4),
      new ZoomInfo("500%",5),
      new ZoomInfo("800%",8),
      new ZoomInfo("1600%",16)
  };

  private J3DTrend          trend;
  private double[]          data;
  private String            sourceName;
  private String            sourceUnit;
  private JScrollPane       trendView;
  private int[]             gColormap;
  private Gradient          gColor;
  private double            zMin;
  private double            zMax;
  private boolean           zAutoScale;
  private JGradientViewer   gradientViewer;
  private JLabel            statusLabel;
  private Color             NaNColor = new Color(128,128,128);
  private int               rdimx;
  private int               rdimy;
  private int               vZoom;
  private int               hZoom;
  private boolean           logScale=false;
  private String            format;

  // Contextual menu
  private boolean           showingMenu;
  protected JPopupMenu      popupMenu;
  protected JMenuItem       saveFileMenuItem;
  protected JMenuItem       settingsMenuItem;
  protected JMenuItem       hProfileMenuItem;
  protected JMenuItem       vProfileMenuItem;
  protected JMenu           hZoomMenu;
  protected JMenu           vZoomMenu;

  // Settings panel
  protected JFrame        settingsFrame = null;
  private JPanel          settingsPanel;
  private JButton         propButton;
  private JCheckBox       autoScaleCheck;
  private JTextField      minText;
  private JTextField      maxText;
  private JGradientEditor gradEditor;
  private JButton         gradButton;
  private JComboBox       hZoomCombo = null;
  private JComboBox       vZoomCombo = null;
  private JButton         okButton;
  private JButton         cancelButton;
  private JCheckBox       logScaleCheck;

  private JFrame              vProfiler = null;
  private JLChart             vProfilerGraph;
  private JLDataView          vProfilerData;
  
  private JFrame              hProfiler = null;
  private JLChart             hProfilerGraph;
  private JLDataView          hProfilerData;
  
  private File                currentFile;
  
  private MainPanel mp;

  /**
   * Construct a number TBTImageViewer 3D viewer
   */
  public TBTImageViewer(MainPanel mp)
  {

    this.mp = mp;
    setLayout(new BorderLayout());

    // Initialise default parameters
    zAutoScale = true;
    zMin = 0.0;
    zMax = 100.0;
    hZoom = 3;
    vZoom = 1;
    gColor = new Gradient();
    gColor.buildRainbowGradient();
    gColormap = gColor.buildColorMap(65536);

    // Main panel components
    trend = new J3DTrend();    
    trend.setParent(this);
    trend.addMouseListener(this);
    trendView = new JScrollPane(trend);
    add(trendView, BorderLayout.CENTER);
    gradientViewer = new JGradientViewer();
    gradientViewer.setGradient(gColor);
    add(gradientViewer, BorderLayout.EAST);
    statusLabel = new JLabel(" ");
    add(statusLabel,BorderLayout.NORTH);

    // Contextual menu
    showingMenu = true;
    popupMenu = new JPopupMenu();

    hZoomMenu = new JMenu("Horz. Zoom");
    popupMenu.add(hZoomMenu);
    for(int i=0;i<zoomInfos.length;i++) {
      JCheckBoxMenuItem m = new JCheckBoxMenuItem(zoomInfos[i].text);
      m.addActionListener(this);
      hZoomMenu.add(m);
    }

    vZoomMenu = new JMenu("Vert. Zoom");
    for(int i=0;i<zoomInfos.length;i++) {
      JCheckBoxMenuItem m = new JCheckBoxMenuItem(zoomInfos[i].text);
      m.addActionListener(this);
      vZoomMenu.add(m);
    }

    popupMenu.add(vZoomMenu);
    popupMenu.add(vZoomMenu);
    popupMenu.add(new JSeparator());
    hProfileMenuItem = new JMenuItem("Horz. profile");
    hProfileMenuItem.addActionListener(this);
    popupMenu.add(hProfileMenuItem);
    vProfileMenuItem = new JMenuItem("Vert. profile");
    vProfileMenuItem.addActionListener(this);
    popupMenu.add(vProfileMenuItem);
    popupMenu.add(new JSeparator());
    settingsMenuItem = new JMenuItem("Settings");
    settingsMenuItem.addActionListener(this);
    popupMenu.add(settingsMenuItem);
    popupMenu.add(new JSeparator());
    saveFileMenuItem = new JMenuItem("Save data");
    saveFileMenuItem.addActionListener(this);
    popupMenu.add(saveFileMenuItem);

    clearData();

  }

  /**
   * Returns 3D trend components
   * @return 3D trend viewer
   */
  public J3DTrend getTrend() {
    return trend;
  }

  /**
   * Clear the viewer
   */
  public void clearData() {
    setData(null,mp.TOTALBPM,100,"","","");
    trend.clearCursor();
  }


  /**
   * Fill the viewer with arbitrary data
   * @param dates Time stamps
   * @param data Data
   */
  public void setData(double[] data,int width,int height,String srcName,String srcUnit,String format) {

    rdimx = width;
    rdimy = height;
    sourceName = srcName;
    sourceUnit = srcUnit;
    this.format = format;
        
    if( data==null ) {

      synchronized (this) {

        // Clear the data        
        double[] newData = new double[width*height];
        for (int i=0; i < width*height; i++) {
          newData[i] = Double.NaN;
        }
        this.data = newData;
        buildImage();
        return;

      }

    }

    synchronized (this) {

      this.data = data;
      buildImage();

    }

  }

  /**
   * Sets the minimum of the z axis (color)
   * @param min Minimum value
   */
  public void setZMinimum(double min) {

    synchronized (this) {
      zMin = min;
    }

  }

  /**
   * Returns the minimum of the of the z axis (color)
   * @return minimum value
   */
  public double getZMinimum() {
    return zMin;
  }

  /**
   * Sets the maximum of the z axis (color)
   * @param max Maximum value
   */
  public void setZMaximum(double max) {

    synchronized (this) {
      zMax = max;
    }

  }

  /**
   * @return maximum value of the z axis (color)
   */
  public double getZMaximum() {
    return zMax;
  }

  /**
   * @return true if the viewer is in autoscale mode for the colormap, false otherwise
   */
  public boolean isZAutoScale() {
    return zAutoScale;
  }

  /**
   * Sets the viewer in autoscale mode for the colormap when true, use min and max otherwise
   * @param autoScale AutoScale flag
   */
  public void setZAutoScale(boolean autoScale) {

    synchronized (this) {
      zAutoScale = autoScale;
    }

  }

  /**
   * @return true if the viewer is in log scale for the colormap, false otherwise
   */
  public boolean isLogScale() {
    return logScale;
  }

  /**
   * Set the viewer in linear or log scale for the colormap
   * @param logScale LogScale flag
   */
  public void setLogScale(boolean logScale) {

    synchronized (this) {
      this.logScale = logScale;
    }

  }

  /**
   * Sets the colormap.
   * @param g Gradient colormap
   */
  public void setGradient(Gradient g) {

    synchronized (this) {
      gColor = g;
      gColormap = g.buildColorMap(65536);
      gradientViewer.setGradient(gColor);
      gradientViewer.repaint();
    }

  }

  /**
   * @return the gradient used by the viewer.
   */
  public Gradient getGradient() {
    return gColor;
  }

  /**
   * Displays or hides the gradient (right panel).
   * @param b True if status line is displayed
   */
  public void setGradientVisible(boolean b) {
    gradientViewer.setVisible(b);
  }

  /**
   * @return true when the gradient is visible.
   */
  public boolean isGradientVisible() {
    return gradientViewer.isVisible();
  }

  /**
   * Sets the color for the NaN values
   * @param nanColor NaN color
   */
  public void setNaNColor(Color nanColor) {
    NaNColor = nanColor;
  }

  /**
   * @return a handle to the horizontal axis
   */
  public JLAxis getXAxis() {
    return trend.getXAxis();
  }

  /**
   * @return a handle to the vertical axis
   */
  public JLAxis getYAxis() {
    return trend.getYAxis();
  }

  /**
   * Sets the horizontal zoom factor
   * See ZoomInfo for accepted zoom.
   * @param zoom zoom factor
   */
  public void setHorizontalZoom(int zoom) {
    synchronized (this) {
      setZoomCombo(hZoomCombo,zoom);
      hZoom=zoom;
    }
  }

  /**
   * @return the horizontal zoom factor.
   */
  public int getHorizontalZoom() {
    return hZoom;
  }

  /**
   * Sets the vertical zoom factor
   * See ZoomInfo for accepted zoom.
   * @param zoom zoom factor
   */
  public void setVerticalZoom(int zoom) {
    synchronized (this) {
      setZoomCombo(vZoomCombo,zoom);
      vZoom=zoom;
    }
  }

  /**
   * @return the vertical zoom factor
   */
  public int getVerticalZoom() {
    return vZoom;
  }
  
  public void setFormat(String f) {
    format = f;
  }

  public String getFormat() {
    return format;
  }

  /**
   * Sets the cursor position (data coordinates)
   * @param x X cursor coordinates
   * @param y Y cursor coordinates
   */
  public void setCursorPos(int x,int y) {

    if(x==-1)
      x = xFromImage(trend.getXCursor());
    if(y==-1)
      y = yFromImage(trend.getYCursor());   
    trend.setCursor(xFromData(x),yFromData(y));

  }

  /**
   * @return horizontal position of the cursor (data coordinates), -1 is returned if there is no cursor.
   */
  public int getXCursor() {

    int x = trend.getXCursor();
    if(x<0) return -1;
    return xFromImage(x);

  }

  /**
   * @return vertical position of the cursor (data coordinates)
   * -1 is returned if there is no cursor.
   */
  public int getYCursor() {

    int y = trend.getYCursor();
    if(y<0) return -1;
    return yFromImage(y);

  }

  /**
   * Return the value at (x,y) position. NaN is returned if no data.
   * @param x X coordinates (in image coordinates)
   * @param y Y coordinates (in image coordinates)
   * @return The value at x,y
   */
  public double getValueAt(int x,int y) {

    int xData = xFromImage(x);
    int yData = yFromImage(y);
    
    if(xData<0 || xData>=rdimx)
      return Double.NaN;
    
    if(yData<0 || yData>=rdimy)
      return Double.NaN;
    
    return data[yData*rdimx + xData];

  }

  /**
   * Enable cross cursor on mouse click
   * @param enable Enable cursor
   */
  public void setCursorEnabled(boolean enable) {
    trend.setCursorEnabled(enable);
  }

  /**
   * Clear the values in the status label, only name is printed until the next click.
   */
  public void clearStatusLabel() {

    trend.clearCursor();

  }
  
    
  /**
   * Update cursor information. This function is trigerred when the user
   * click on the image.
   * @param xCursor x coordinates (referenced by the image)
   * @param yCursor y coordinates (referenced by the image)
   * @param fireCursorChange Fire cursorChange event
   */
  public void updateCursor(int xCursor, int yCursor,boolean fireCursorChange) {

    synchronized (this) {

      if (xCursor < 0) {
        
        statusLabel.setText(sourceName);
        
      } else {
        
        int bpm = xFromImage(xCursor);
        int turn = yFromImage(yCursor);
          
        if(bpm<0 || bpm>=rdimx || turn<0 || turn>=rdimy) {
            
          statusLabel.setText(sourceName + " | No data at marker position");             
            
        } else {

          double val = getValueAt(xCursor, yCursor);
          statusLabel.setText(
                  sourceName + " BPM " + getBPMName(bpm) + 
                  " Turn #" + Integer.toString(turn+1)+ ": " + 
                  getFormatedVal(val));
          
        }

      }

      refreshProfile();

    }


  }
  
  public void refreshProfile() {
    
      // Refresh profile
      if (vProfiler != null) {
        if (vProfiler.isVisible()) {
          buildVerticalProfile();
        }
      }

      if (hProfiler != null) {
        if (hProfiler.isVisible()) {
          buildHorizontalProfile();
        }
      }
      
  }

  /**
   * True to enable menu displayed when clicking on right mouse button.
   * @param b True to enable the menu
   */
  public void setShowingMenu(boolean b) {
    showingMenu = b;
  }

  /**
   * Returns true is the image viewer menu is displayed when clicking
   * on the right mouse button.
   * @return True is menu is enabled
   */
  public boolean isShowingMenu() {
    return showingMenu;
  }

  /**
   * Rebuild image when some settings has been changed.
   */
  public void commit() {

    synchronized (this) {
      buildImage();
    }
    repaint();

  }

  /**
   * Scrolls the image to the right
   */
  public void scrollToRight() {
    JScrollBar bar = trendView.getHorizontalScrollBar();
    int p = bar.getMaximum();
    bar.setValue(p);
  }

  /**
   * Scrolls the image to the left
   */
  public void scrollToLeft() {
    JScrollBar bar = trendView.getHorizontalScrollBar();
    int p = bar.getMinimum();
    bar.setValue(p);
  }

  /**
   * Sets the horizontal scroll position
   * @param pos Scrollbar position
   */
  public void setHorinzontalScrollPos(int pos) {
    JScrollBar bar = trendView.getHorizontalScrollBar();
    bar.setValue(pos);
  }

  /**
   * Sets the vertical scroll position
   * @param pos Scrollbar position
   */
  public void setVertitalScrollPos(int pos) {
    JScrollBar bar = trendView.getVerticalScrollBar();
    bar.setValue(pos);
  }

  /**
   * Sets the trend backgroudn color
   * @param bg Background color
   */
  public void setTrendBackground(Color bg) {
    trend.setBackground(bg);
  }

  // ------------------------------------------------------------------------
  // Action listener
  // ------------------------------------------------------------------------

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if( src==settingsMenuItem ) {
      showSettings();
    } else if ( src==saveFileMenuItem ) {
      saveDataFile();
    } else if ( src==autoScaleCheck ) {
      applyAutoScale();
    } else if ( src==cancelButton ) {
      settingsFrame.setVisible(false);
    } else if ( src==okButton ) {
      applySettings();
    } else if ( src==minText ) {
      applyMinMaxAndBuild();
    } else if ( src==maxText ) {
      applyMinMaxAndBuild();
    } else if ( src==gradButton ) {
      showGradientEditor();
    } else if ( src==hZoomCombo ) {
      applyHorizontalZoom();
    } else if ( src==vZoomCombo ) {
      applyVerticalZoom();
    } else if ( src==vProfileMenuItem ) {
      showVerticalProfile();
    } else if ( src==hProfileMenuItem ) {
      showHorizontalProfile();
    } else if ( src==logScaleCheck ) {
      applyLogScale();
    } else {
      // Zoom menu
      int hIdx = foundZoomMenu(hZoomMenu,src);
      if(hIdx>=0) applyHorizontalZoom(hIdx);
      int vIdx = foundZoomMenu(vZoomMenu,src);
      if(vIdx>=0) applyVerticalZoom(vIdx);
    }
    
  }

  // ------------------------------------------------------------------------
  // Chart listener
  // ------------------------------------------------------------------------

  public String[] clickOnChart(JLChartEvent evt) {

    String[] ret = new String[0];

    Object src = evt.getSource();

    if( src==hProfilerGraph ) {

      double val = evt.getTransformedYValue();
      int bpm = (int)evt.getTransformedXValue();

      ret = new String[2];
      ret[0] = "BPM " + getBPMName(bpm);
      ret[1] = sourceName + "=" + getFormatedVal(val);

    } else if( src==vProfilerGraph ) {
      
      double val = evt.getTransformedYValue();
      int turn = (int)evt.getTransformedXValue();

      ret = new String[2];
      ret[0] = "Turn #" + Integer.toString(turn);
      ret[1] = sourceName + "=" + getFormatedVal(val);
      
    }

    return ret;

  }

  // ------------------------------------------------------------------------
  // MouseListener listener
  // ------------------------------------------------------------------------

  public void mouseClicked(MouseEvent e) {}

  public void mousePressed(MouseEvent e) {

    if(e.getButton()==MouseEvent.BUTTON3) {
      if (showingMenu && e.getSource()==trend) {
        hProfileMenuItem.setEnabled(trend.isCursorInside());
        vProfileMenuItem.setEnabled(trend.isCursorInside());
        setZoomMenu(hZoomMenu, hZoom);
        setZoomMenu(vZoomMenu,vZoom);
        popupMenu.show(trend, e.getX() , e.getY());
      }
    }

  }

  public void mouseReleased(MouseEvent e) {}

  public void mouseEntered(MouseEvent e) {}

  public void mouseExited(MouseEvent e) {}

  // ------------------------------------------------------------------
  // private stuff
  // ------------------------------------------------------------------

  private int xFromImage(int xImg) {
    
    int xData;
    if( hZoom>=1 ) {
      xData = xImg/hZoom;
    } else {
      xData = xImg*(-hZoom);
    }
    return xData;
    
  }
  
  private int yFromImage(int yImg) {
    
    int yData;
    if( vZoom>=1 ) {
      yData = yImg/vZoom;
    } else {
      yData = yImg*(-vZoom);
    }
    return yData;

  }
  
  private int xFromData(int x) {
    
    int xImg;

    if(hZoom>=1) {
      xImg = hZoom * x;
    } else {
      xImg = x/(-hZoom);
    }
    
    return xImg;
    
  }
  
  private int yFromData(int y) {
    
    int yImg;

    if(vZoom>=1) {
      yImg = vZoom * y;
    } else {
      yImg =y/(-vZoom);
    }
    
    return yImg;

  }
  
  private String getBPMName(int idx) {
    
    int cell = (idx/mp.NBBPM+3)%MainPanel.NBCELL + 1;
    int bpm = idx % mp.NBBPM;
    return "C" + String.format("%02d", cell) + "-" + mp.BPMIDX[bpm] + " [" + mp.BPMTYPE[bpm] + "]";
  
  }
  
  private String getFormatedVal(double val) {
    
    if(Double.isNaN(val))
      return "NaN";
    
    if(format.length()==0) {
      return Double.toString(val) + " " + sourceUnit;
    } else {
      return ATKFormat.format(format, val) + " " + sourceUnit;
    }
    
  }

  // -- Profile plotter -----------------------------------------------

  public void showVerticalProfile() {
    
    constructVerticalProfiler();
    synchronized (this) {
      buildVerticalProfile();
    }
    if(!vProfiler.isVisible())
      ATKGraphicsUtils.centerFrameOnScreen(vProfiler);
    vProfiler.setVisible(true);

  }

  public void showHorizontalProfile() {

    constructHorizontalProfiler();
    synchronized (this) {
      buildHorizontalProfile();
    }
    if(!hProfiler.isVisible())
      ATKGraphicsUtils.centerFrameOnScreen(hProfiler);
    hProfiler.setVisible(true);

  }
  
  private void buildVerticalProfile() {

    int x = xFromImage(trend.getXCursor());
    String title = "Turn by turn profile";
    if(sourceUnit.length()==0)
      vProfilerGraph.getY1Axis().setName(sourceName);
    else
      vProfilerGraph.getY1Axis().setName(sourceName + " (" + sourceUnit + ")");
      

    vProfilerData.reset();
    if(x>=0 && x<rdimx) {
      title += " for BPM " + getBPMName(x);
      for(int i=0;i<rdimy;i++) {
        vProfilerData.add(i,data[x + i*rdimx]);
      }
    }
    
    vProfilerGraph.setHeader(title);      
    vProfilerGraph.repaint();

  }

  private void buildHorizontalProfile() {

    int y = yFromImage(trend.getYCursor());
    String title = "Orbit Plot";
    if(sourceUnit.length()==0)
      hProfilerGraph.getY1Axis().setName(sourceName);
    else
      hProfilerGraph.getY1Axis().setName(sourceName + " (" + sourceUnit + ")");

    hProfilerData.reset();

    if(y>=0 && y<rdimy) {
      title += " at turn #" + (y+1);
      for(int i=0;i<rdimx;i++) {
        hProfilerData.add(i,data[i + y*rdimx]);
      }
    }

    hProfilerGraph.setHeader(title);      
    hProfilerGraph.repaint();

  }

  private void constructHorizontalProfiler() {

    if (hProfiler == null) {

      JPanel innerPanel = new JPanel(new BorderLayout());

      hProfilerGraph = new JLChart();
      hProfilerGraph.setBorder(new javax.swing.border.EtchedBorder());
      hProfilerGraph.getXAxis().setAutoScale(true);
      hProfilerGraph.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
      hProfilerGraph.getXAxis().setGridVisible(true);
      hProfilerGraph.getXAxis().setName("BPM");
      hProfilerGraph.getY1Axis().setAutoScale(true);
      hProfilerGraph.getY1Axis().setGridVisible(true);
      hProfilerGraph.setPreferredSize(new Dimension(600, 400));
      hProfilerGraph.setMinimumSize(new Dimension(600, 400));
      hProfilerGraph.setHeaderFont(new Font("Dialog",Font.BOLD,18));
      hProfilerGraph.setLabelVisible(false);
      hProfilerGraph.setJLChartListener(this);
      innerPanel.add(hProfilerGraph, BorderLayout.CENTER);

      hProfilerData = new JLDataView();
      hProfilerGraph.getY1Axis().addDataView(hProfilerData);

      hProfiler = new JFrame();
      hProfiler.setTitle("TBT Orbit");
      hProfiler.setContentPane(innerPanel);

    }

  }

  private void constructVerticalProfiler() {
    
    if (vProfiler == null) {

      JPanel innerPanel = new JPanel(new BorderLayout());

      vProfilerGraph = new JLChart();
      vProfilerGraph.setBorder(new javax.swing.border.EtchedBorder());
      vProfilerGraph.getXAxis().setAutoScale(true);
      vProfilerGraph.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
      vProfilerGraph.getXAxis().setGridVisible(true);
      vProfilerGraph.getXAxis().setName("Turn");
      vProfilerGraph.getY1Axis().setAutoScale(true);
      vProfilerGraph.getY1Axis().setGridVisible(true);
      vProfilerGraph.setPreferredSize(new Dimension(600, 400));
      vProfilerGraph.setMinimumSize(new Dimension(600, 400));
      vProfilerGraph.setHeaderFont(new Font("Dialog",Font.BOLD,18));
      vProfilerGraph.setHeader("Turn Plot");
      vProfilerGraph.setLabelVisible(false);
      vProfilerGraph.setJLChartListener(this);
      innerPanel.add(vProfilerGraph, BorderLayout.CENTER);

      vProfilerData = new JLDataView();
      vProfilerGraph.getY1Axis().addDataView(vProfilerData);

      vProfiler = new JFrame();
      vProfiler.setTitle("Turn by turn profile");
      vProfiler.setContentPane(innerPanel);

    }


  }

  // -- Settings panel ------------------------------------------------------------------

  public void showSettings() {

    constructSettingsPanel();
    initSettings();
    if( !settingsFrame.isVisible() ) {
      ATKGraphicsUtils.centerFrame(this,settingsFrame);
    }
    settingsFrame.setVisible(true);

  }

  private int getZoomIndex(int zoom) {

    boolean found = false;
    int i = 0;
    while(!found && i<zoomInfos.length) {
      found = zoomInfos[i].ratio == zoom;
      if(!found) i++;
    }

    if(found) {
      return i;
    } else {
      throw new IllegalStateException("Unexpected zoom ratio: " + Integer.toHexString(zoom));
    }

  }

  private void setZoomCombo(JComboBox combo,int zoom) {

    int zIdx = getZoomIndex(zoom);
    if(combo!=null) combo.setSelectedIndex(zIdx);

  }

  private int foundZoomMenu(JMenu menu,Object src) {

    boolean found = false;
    int i = 0;
    while(!found && i<menu.getMenuComponentCount()) {
      found = menu.getMenuComponent(i).equals( src );
      if(!found) i++;
    }

    if(found)
      return i;
    else
      return -1;

  }

  private void setZoomMenu(JMenu menu,int zoom) {

    int idx = getZoomIndex(zoom);
    for(int i=0;i<menu.getMenuComponentCount();i++) {
      JCheckBoxMenuItem m = (JCheckBoxMenuItem)menu.getMenuComponent(i);
      m.setSelected(idx==i);
    }

  }

  private int getZoomCombo(JComboBox combo) {

    int s = combo.getSelectedIndex();
    return zoomInfos[s].ratio;

  }

  private void initSettings(){

    autoScaleCheck.setSelected(zAutoScale);
    minText.setEnabled(!zAutoScale);
    minText.setText(Double.toString(zMin));
    maxText.setEnabled(!zAutoScale);
    maxText.setText(Double.toString(zMax));
    gradEditor.setGradient(gColor);
    setZoomCombo(vZoomCombo,vZoom);
    setZoomCombo(hZoomCombo,hZoom);
    logScaleCheck.setSelected(logScale);

  }

  private void applySettings() {

    applyMinMax();
    synchronized (this) {
      buildImage();
    }
    repaint();

  }

  private void applyMinMax() {

    double min,max;

    try {
      min = Double.parseDouble(minText.getText());
    } catch (NumberFormatException e1) {
      JOptionPane.showMessageDialog(this,"Invalid entry for min\n"+e1.getMessage());
      return;
    }

    try {
      max = Double.parseDouble(maxText.getText());
    } catch (NumberFormatException e2) {
      JOptionPane.showMessageDialog(this,"Invalid entry for max\n"+e2.getMessage());
      return;
    }

    if(min>=max) {
      JOptionPane.showMessageDialog(this,"min must be lower than max\n");
      return;
    }

    if( logScale && !zAutoScale ) {
      if(min<=0 || max<=0) {
        JOptionPane.showMessageDialog(this,"min and max must be strictly positive in log scale\n");
        return;
      }
    }

    synchronized (this) {
      zMin = min;
      zMax = max;
    }

  }

  private void applyMinMaxAndBuild() {

    applyMinMax();
    synchronized (this) {
      buildImage();
    }
    repaint();

  }

  private void applyLogScale() {

    if(!zAutoScale) {
      if(zMin<=0 || zMax<=0) {
        JOptionPane.showMessageDialog(this,"min and max must be strictly positive in log scale\n");
        logScaleCheck.setSelected(false);
        logScale = false;
        return;
      }
    }

    synchronized (this) {
      logScale = logScaleCheck.isSelected();
      buildImage();
    }
    repaint();

  }

  private void applyAutoScale() {

    synchronized (this) {
      zAutoScale = autoScaleCheck.isSelected();
      if(!zAutoScale) {
        if (logScale) {
          if (zMin <= 0 || zMax <= 0) {
            JOptionPane.showMessageDialog(this, "min and max must be strictly positive in log scale\n");
            zAutoScale = true;
            autoScaleCheck.setSelected(true);
            return;
          }
        }
      }
      minText.setEnabled(!zAutoScale);
      maxText.setEnabled(!zAutoScale);
      buildImage();
    }
    repaint();

  }

  private void applyHorizontalZoom() {
    synchronized (this) {
      hZoom = getZoomCombo(hZoomCombo);
      buildImage();
    }
    repaint();
  }

  private void applyVerticalZoom() {
    synchronized (this) {
      vZoom =  getZoomCombo(vZoomCombo);
      buildImage();
    }
    repaint();
  }

  private void applyHorizontalZoom(int zIdx) {
    synchronized (this) {
      hZoom = zoomInfos[zIdx].ratio;
      if(hZoomCombo!=null) setZoomCombo(hZoomCombo,hZoom);
      buildImage();
    }
    repaint();
  }

  private void applyVerticalZoom(int zIdx) {
    synchronized (this) {
      vZoom = zoomInfos[zIdx].ratio;
      if(vZoomCombo!=null) setZoomCombo(vZoomCombo,vZoom);
      buildImage();
    }
    repaint();
  }

  private void showGradientEditor() {

    Gradient g = JGradientEditor.showDialog(settingsFrame, gColor);
    if (g != null) {
      gColor = g;
      gColormap = g.buildColorMap(65536);
      gradEditor.setGradient(gColor);
      gradEditor.repaint();
      gradientViewer.setGradient(gColor);
      gradientViewer.repaint();
    }

  }

  private void constructSettingsPanel() {

    if (settingsFrame == null) {

      // ------------------------------------------------------
      // Settings panel
      // ------------------------------------------------------
      settingsPanel = new JPanel();
      settingsPanel.setLayout(null);
      settingsPanel.setMinimumSize(new Dimension(290, 290));
      settingsPanel.setPreferredSize(new Dimension(290, 290));

      propButton = new JButton();
      propButton.setText("?");
      propButton.setToolTipText("Edit attribute properties");
      propButton.setFont(ATKConstant.labelFont);
      propButton.setMargin(new Insets(0, 0, 0, 0));
      propButton.setBounds(250, 5, 30, 30);
      propButton.addActionListener(this);
      settingsPanel.add(propButton);

      // ------------------------------------------------------------------------------------
      JSeparator js = new JSeparator();
      js.setBounds(0, 40, 500, 10);
      settingsPanel.add(js);

      autoScaleCheck = new JCheckBox("Auto scale");
      autoScaleCheck.setFont(ATKConstant.labelFont);
      autoScaleCheck.setBounds(5, 52, 100, 20);
      autoScaleCheck.setToolTipText("Auto scale colormap");
      autoScaleCheck.addActionListener(this);
      settingsPanel.add(autoScaleCheck);

      JLabel minLabel = new JLabel("Min");
      minLabel.setFont(ATKConstant.labelFont);
      minLabel.setBounds(110,50,30,25);
      settingsPanel.add(minLabel);

      minText = new JTextField();
      minText.setBounds(145,50,50,25);
      minText.addActionListener(this);
      settingsPanel.add(minText);

      JLabel maxLabel = new JLabel("Max");
      maxLabel.setFont(ATKConstant.labelFont);
      maxLabel.setBounds(200,50,30,25);
      settingsPanel.add(maxLabel);

      maxText = new JTextField();
      maxText.setBounds(235,50,50,25);
      maxText.addActionListener(this);
      settingsPanel.add(maxText);

      JLabel gradLabel = new JLabel("Colormap");
      gradLabel.setFont(ATKConstant.labelFont);
      gradLabel.setBounds(5, 80, 70, 20);
      settingsPanel.add(gradLabel);

      gradEditor = new JGradientEditor();
      gradEditor.setEditable(false);
      gradEditor.setToolTipText("Display the image using this colormap");
      gradEditor.setBounds(80, 80, 180, 20);
      settingsPanel.add(gradEditor);

      gradButton = new JButton();
      gradButton.setText("...");
      gradButton.setToolTipText("Edit colormap");
      gradButton.setFont(ATKConstant.labelFont);
      gradButton.setMargin(new Insets(0, 0, 0, 0));
      gradButton.setBounds(260, 80, 25, 20);
      gradButton.addActionListener(this);
      settingsPanel.add(gradButton);

      // ------------------------------------------------------------------------------------
      JSeparator js3 = new JSeparator();
      js3.setBounds(0, 150, 500, 10);
      settingsPanel.add(js3);

      JLabel hZoomLabel = new JLabel("Horz. zoom");
      hZoomLabel.setFont(ATKConstant.labelFont);
      hZoomLabel.setBounds(5, 160, 70, 20);
      settingsPanel.add(hZoomLabel);

      hZoomCombo = new JComboBox();
      hZoomCombo.setFont(ATKConstant.labelFont);
      for(int i=0;i<zoomInfos.length;i++)
        hZoomCombo.addItem(zoomInfos[i].text);
      hZoomCombo.setBounds(80, 160, 60, 22);
      hZoomCombo.addActionListener(this);
      settingsPanel.add(hZoomCombo);

      JLabel vZoomLabel = new JLabel("Vert. zoom");
      vZoomLabel.setFont(ATKConstant.labelFont);
      vZoomLabel.setBounds(150, 160, 70, 20);
      settingsPanel.add(vZoomLabel);

      vZoomCombo = new JComboBox();
      vZoomCombo.setFont(ATKConstant.labelFont);
      for(int i=0;i<zoomInfos.length;i++)
        vZoomCombo.addItem(zoomInfos[i].text);
      vZoomCombo.setBounds(225, 160, 60, 22);
      vZoomCombo.addActionListener(this);
      settingsPanel.add(vZoomCombo);

      // ------------------------------------------------------------------------------------
      JSeparator js4 = new JSeparator();
      js4.setBounds(0, 190, 500, 10);
      settingsPanel.add(js4);

      logScaleCheck = new JCheckBox("Display log values");
      logScaleCheck.setFont(ATKConstant.labelFont);
      logScaleCheck.setBounds(5, 200, 150, 20);
      logScaleCheck.setToolTipText("Display log values");
      logScaleCheck.addActionListener(this);
      settingsPanel.add(logScaleCheck);
      
      // ------------------------------------------------------------------------------------

      okButton = new JButton();
      okButton.setText("Apply");
      okButton.setFont(ATKConstant.labelFont);
      okButton.setBounds(5, 260, 90, 25);
      okButton.addActionListener(this);
      settingsPanel.add(okButton);

      cancelButton = new JButton();
      cancelButton.setText("Dismiss");
      cancelButton.setFont(ATKConstant.labelFont);
      cancelButton.setBounds(195, 260, 90, 25);
      cancelButton.addActionListener(this);
      settingsPanel.add(cancelButton);

      settingsFrame = new JFrame();
      settingsFrame.setTitle("NumberSpectrumTrend Options");
      settingsFrame.setContentPane(settingsPanel);

    }

  }

  private double computeHighTen(double d) {
    int p = (int)Math.log10(d);
    return Math.pow(10.0, p + 1);
  }

  private double computeLowTen(double d) {
    int p = (int)Math.log10(d);
    return Math.pow(10.0, p);
  }


  private void buildImage() {

    // Compute ymax, zmax and zmin
    boolean zRangeOK = false;
    double min = Double.MAX_VALUE;
    double max = -Double.MAX_VALUE;
    if(!zAutoScale) {
      max = zMax;
      min = zMin;
    } else {
      int length = rdimx * rdimy;
      for (int i = 0; i < length; i++) {
        if (!Double.isNaN(data[i])) {
          double v = data[i];
          if (v < min) min = v;
          if (v > max) max = v;
          zRangeOK = true;
        }
      }        
    }
    
    if( zRangeOK ) {
      if( logScale ) {
        if ((max - min) < 1e-100) {
          max = computeHighTen(max);
          min = computeLowTen(min);
        }
      } else {
        if ((max - min) < 1e-100) {
          max += 0.999;
          min -= 0.999;
        }
      }
    } else {
      // Only Nan or invalid data
      min = zMin;
      max = zMax;
    }

    // Update gradient viewer
    gradientViewer.getAxis().setMinimum(min);
    gradientViewer.getAxis().setMaximum(max);
    if( logScale ) {
      gradientViewer.getAxis().setScale(JLAxis.LOG_SCALE);
    } else {
      gradientViewer.getAxis().setScale(JLAxis.LINEAR_SCALE);
    }

    // Update image
    BufferedImage lastImg = trend.getImage();

    int dimx;
    if(hZoom>=1) {
      dimx = rdimx*hZoom;
    } else {
      dimx = rdimx/(-hZoom);
    }
    int dimy;
    if(vZoom>=1) {
      dimy = rdimy*vZoom;
    } else {
      dimy = rdimy/(-vZoom);
    }

    if (lastImg == null || lastImg.getHeight() != dimy || lastImg.getWidth() != dimx) {
      // Recreate the image
      lastImg = new BufferedImage(dimx, dimy, BufferedImage.TYPE_INT_RGB);
      trend.setImage(lastImg,hZoom,vZoom);
    }

    if (zAutoScale && !zRangeOK) {

      // Only error or not initialized data
      Graphics2D g = (Graphics2D) lastImg.getGraphics();
      g.setColor(NaNColor);
      g.fillRect(0, 0, dimx, dimy);

    } else {

      if (max - min < 1e-20) max += 1.0;
      if (logScale) {
        min = Math.log10(min);
        max = Math.log10(max);
      }

      int rgbNaN = NaNColor.getRGB();
      int[] rgb = new int[dimx];

      if (vZoom >= 1 && hZoom >= 1) {

        // X zoom, Y zoom --------------------------------------------------------------------------
        for (int j = 0; j < rdimy; j++) {

          for (int i = 0; i < rdimx; i++) {

            double v = data[i + j * rdimx];
            if (Double.isNaN(v)) {
              
              for (int i2 = 0; i2 < hZoom; i2++)
                rgb[hZoom * i + i2] = rgbNaN;
              
            } else {
              
              double c;
              if (logScale)
                c = ((Math.log10(v) - min) / (max - min)) * 65536.0;
              else
                c = ((v - min) / (max - min)) * 65536.0;

              if (c < 0.0) c = 0.0;
              if (c > 65535.0) c = 65535.0;
              for (int i2 = 0; i2 < hZoom; i2++)
                rgb[hZoom * i + i2] = gColormap[(int) c];
              
            }

          }

          for (int j2 = 0; j2 < vZoom; j2++)
            lastImg.setRGB(0, vZoom * j + j2, dimx, 1, rgb, 0, dimx);
          
        }

      } else if (vZoom >= 1 && hZoom < 0) {

        // X unzoom, Y zoom ----------------------------------------------------------------------------
        for (int j = 0; j < rdimy; j++) {

          for (int i = 0; i < rdimx; i += (-hZoom)) {

            double v = data[i + j * rdimx];
            int xpos = i / (-hZoom);

            if (Double.isNaN(v)) {
              if (xpos < dimx) rgb[xpos] = rgbNaN;
            } else {
              double c;
              if (logScale)
                c = ((Math.log10(v) - min) / (max - min)) * 65536.0;
              else
                c = ((v - min) / (max - min)) * 65536.0;

              if (c < 0.0) c = 0.0;
              if (c > 65535.0) c = 65535.0;
              if (xpos < dimx)
                rgb[xpos] = gColormap[(int) c];
            }

          }

          for (int j2 = 0; j2 < vZoom; j2++)
            lastImg.setRGB(0, vZoom * j + j2, dimx, 1, rgb, 0, dimx);

        }

      } else if (vZoom < 0 && hZoom >= 1) {

        // X zoom, Y unzoom --------------------------------------------------------------------------
        for (int j = 0; j < rdimy; j+=(-vZoom)) {

          for (int i = 0; i < rdimx; i++) {
                double v = data[i + j*rdimx];
                if (Double.isNaN(v)) {
                  for (int i2 = 0; i2 < hZoom; i2++) rgb[hZoom * i + i2] = rgbNaN;
                } else {
                  double c;
                  if (logScale)
                    c = ((Math.log10(v) - min) / (max - min)) * 65536.0;
                  else
                    c = ((v - min) / (max - min)) * 65536.0;
                  if (c < 0.0) c = 0.0;
                  if (c > 65535.0) c = 65535.0;
                  for (int i2 = 0; i2 < hZoom; i2++) 
                    rgb[hZoom * i + i2] = gColormap[(int) c];
                }                
          }

          if((j/(-vZoom))<dimy) 
            lastImg.setRGB(0,  j/(-vZoom) , dimx, 1, rgb, 0, dimx);
          
        }

      } else if (vZoom < 0 && hZoom < 0) {

        // X unzoom, Y unzoom ---------------------------------------------------------------------
        for (int j = 0; j < rdimy; j+=(-vZoom)) {

          for (int i = 0; i < rdimx; i+=(-hZoom)) {
            
            double v = data[i + j*rdimx];
            int xpos = i/(-hZoom);
            if (Double.isNaN(v)) {
              if(xpos<dimx) rgb[xpos] = rgbNaN;
            } else {
              
              double c;
              if (logScale)
                c = ((Math.log10(v) - min) / (max - min)) * 65536.0;
              else
                c = ((v - min) / (max - min)) * 65536.0;
              if (c < 0.0) c = 0.0;
              if (c > 65535.0) c = 65535.0;
              if(xpos<dimx) rgb[xpos] = gColormap[(int) c];
            }
          }
          if((j/(-vZoom))<dimy) 
            lastImg.setRGB(0,  j/(-vZoom) , dimx, 1, rgb, 0, dimx);
        }

      }

    }

    trend.setImage(lastImg,rdimx,rdimy);
    trend.getYAxis().setMinimum(0.0);
    trend.getYAxis().setMaximum(rdimy);
    trend.getYAxis().setName("Turn");
    trend.getXAxis().setMinimum(0.0);
    trend.getXAxis().setMaximum(rdimx);
    trend.getXAxis().setName("BPM");
    trendView.getViewport().revalidate();
    revalidate();

  }

  public void saveDataFile() {

    JFileChooser fc = new JFileChooser(".");
    JCheckBox transposeCheck = new JCheckBox("Transpose data");
    transposeCheck.setFont(ATKConstant.labelFont);
    transposeCheck.setSelected(true);
    fc.setAccessory(transposeCheck);
    if (currentFile != null)
      fc.setSelectedFile(currentFile);
    int status = fc.showSaveDialog(this);
    if (status == JFileChooser.APPROVE_OPTION) {
      currentFile = fc.getSelectedFile();
      try {
        FileWriter f = new FileWriter(currentFile);
        f.write(makeTabbedString(transposeCheck.isSelected()));
        f.close();
      } catch (IOException ex) {
        JOptionPane.showMessageDialog(this, ex, "Error while saving data", JOptionPane.ERROR_MESSAGE);
      }
    }

  }


  protected String makeTabbedString(boolean transpose) {

    StringBuffer str = new StringBuffer();
    int nbCol = data.length;
    int nbRow = 0;

    // Compute nbRow

    if( !transpose ) {

      for(int j=0;j<rdimy;j--) {
        for(int i=0;i<rdimx;i++) {
          double v = data[i + j*rdimx];
          str.append( getFormatedVal(v) );
          str.append("\t");
        }
        str.append("\n");
      }

    } else {

      for(int i=0;i<rdimx;i++) {
        for(int j=0;j<rdimy;j--) {
          double v = data[i + j*rdimx];
          str.append( getFormatedVal(v) );
          str.append("\t");
        }
        str.append("\n");
      }

    }

    return str.toString();

  }


}
