package SRBpmManager;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DevStateSpectrumEvent;
import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IDevStateSpectrumListener;
import fr.esrf.tangoatk.core.attribute.DevStateSpectrum;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/**
 * BPM state viewer
 */
public class BPMStateViewer extends JPanel implements IDevStateSpectrumListener {

  private static Font bpiFont = new Font("Dailog",Font.BOLD,12);
  private static Dimension cellSize = new Dimension(40,25);

  private JLabel[][]  states;
  
  private DevStateSpectrum model;
  MainPanel mp;

  public BPMStateViewer(MainPanel mp) {
    
    this.mp = mp;
    
    
    setLayout(new GridLayout(mp.NBBPM+1,MainPanel.NBCELL+1));
        
    boolean[] isBPI = new boolean[mp.TOTALBPM];
    try {
      Device d = DeviceFactory.getInstance().getDevice(mp.BPMALL_DEVNAME);
      isBPI = d.readAttribute("All_BPIStation").extractBooleanArray();
    } catch(Exception e) {
      mp.splash.setVisible(false);
      ErrorPane.showErrorMessage(null, mp.BPMALL_DEVNAME, e);
      System.exit(0);
    }
    
    // Row #0 (Cell label)
    
    add( createLabel("") );
    for(int i=0;i<MainPanel.NBCELL;i++) { 
      String cName = "C" + String.format("%02d", (i+3)%MainPanel.NBCELL + 1);      
      add(createLabel(cName));      
    }
    
    // BPMs
    
    
    states = new JLabel[MainPanel.NBCELL][mp.NBBPM];
    for(int j=0;j<mp.NBBPM;j++) {
      
      add( createLabel("#"+String.format("%02d",mp.BPMIDX[j])) );
      
      for(int i=0;i<MainPanel.NBCELL;i++) { 
        
        JPanel innerPanel = new JPanel();
        innerPanel.setLayout(new BorderLayout());
        states[i][j] = new JLabel();
        states[i][j].setOpaque(true);
        states[i][j].setFont(bpiFont);
        states[i][j].setForeground(Color.BLACK);
        states[i][j].setHorizontalAlignment(JLabel.CENTER);
        if(isBPI[i*mp.NBBPM+j])
          states[i][j].setText("BPI");

        innerPanel.setBorder(BorderFactory.createEtchedBorder());
        innerPanel.setPreferredSize(cellSize);
        innerPanel.add(states[i][j],BorderLayout.CENTER);
        
        final int cellIdx = (i+3)%MainPanel.NBCELL + 1;
        final int bpmIdx = mp.BPMIDX[j];
        final String bpmType = mp.BPMTYPE[j];
        final JLabel srcPanel = states[i][j];
        final String devName = "srdiag/bpm/c" + String.format("%02d", cellIdx) + "-" + String.format("%02d", bpmIdx);
        final JPopupMenu bpmMenu = new JPopupMenu();
        
        JMenuItem enableBPM = new JMenuItem("Enable BPM");
        enableBPM.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            enableBPM(devName,bpmType);       
          }
        });
        bpmMenu.add(enableBPM);
        JMenuItem disableBPM = new JMenuItem("Disable BPM");
        disableBPM.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            disableBPM(devName,bpmType);
          }
        });
        bpmMenu.add(disableBPM);

        states[i][j].addMouseListener(new MouseAdapter() {
           public void mouseClicked(MouseEvent e) {
               
             if( e.getButton()==MouseEvent.BUTTON1 ) {
               launchPanel(devName);
             }
             
             if( e.getButton()==MouseEvent.BUTTON3 ) {
               bpmMenu.show(srcPanel, e.getX(),e.getY());               
             }
             
           }
        });
        
        
        add(innerPanel);
      }
    }
    
  }
  
  private JLabel createLabel(String text) {
    JLabel label = new JLabel(text);
    label.setBorder(BorderFactory.createEtchedBorder());
    label.setPreferredSize(cellSize);
    label.setFont(ATKConstant.labelFont);
    label.setHorizontalAlignment(JLabel.CENTER);
    return label;
  }

  public void setModel(DevStateSpectrum states) {
    clearModel();
    model = states;
    model.addDevStateSpectrumListener(this);    
  }
  
  public void clearModel() {
    if(model!=null)
      model.removeDevStateSpectrumListener(this);
    model = null;
  }

  @Override
  public void devStateSpectrumChange(DevStateSpectrumEvent dsse) {

    String[] values = dsse.getValue();
    
    for(int i=0;i<values.length;i++) {
      int cellIdx = i/mp.NBBPM;
      int bpmIdx = i%mp.NBBPM;
      states[cellIdx][bpmIdx].setBackground(ATKConstant.getColor4State(values[i]));
    }

  }

  @Override
  public void stateChange(AttributeStateEvent ase) {
  }

  @Override
  public void errorChange(ErrorEvent ee) {
    
    for(int i=0;i<mp.TOTALBPM;i++) {
      int cellIdx = i/mp.NBBPM;
      int bpmIdx = i%mp.NBBPM;
      states[cellIdx][bpmIdx].setBackground(ATKConstant.getColor4State("UNKNOWN"));
    }
    
  }
  
  private void sendCommand(Device d,String devName,String command) throws DevFailed {

    DeviceData argin = new DeviceData();
    argin.insert(devName);
    DeviceData argout = d.command_inout("Devicename2Indice",argin);      
    d.command_inout(command, argout);      
    
  }
  
  public void enableBPM(String devName,String type) {

    int disa = 0;    
    
    try {
      
      // Disable on all device
      Device dsall = DeviceFactory.getInstance().getDevice(mp.BPMALL_DISABLE[disa]);
      sendCommand(dsall,devName,"EnableDevice");
      
      if(type.equalsIgnoreCase("Brilliance")) {
        // Disable on brillance all
        disa = 1;
        Device ds = DeviceFactory.getInstance().getDevice(mp.BPMALL_DISABLE[disa]);
        sendCommand(ds,devName,"EnableDevice");
      }
      
      if(type.equalsIgnoreCase("Spark")) {
        // Disable on spark all
        disa = 2;
        Device ds = DeviceFactory.getInstance().getDevice(mp.BPMALL_DISABLE[disa]);
        sendCommand(ds,devName,"EnableDevice");
      }
      
    } catch(DevFailed e1) {
      ErrorPane.showErrorMessage(null,mp.BPMALL_DISABLE[disa], e1);
    } catch(ConnectionException e2) {
      ErrorPane.showErrorMessage(null,mp.BPMALL_DISABLE[disa], e2);      
    }
            
  }
  
  public void disableBPM(String devName,String type) {
    
    int disa = 0;    
    
    try {
      
      // Disable on all device
      Device dsall = DeviceFactory.getInstance().getDevice(mp.BPMALL_DISABLE[disa]);
      sendCommand(dsall,devName,"DisableDevice");
      
      if(type.equalsIgnoreCase("Brilliance")) {
        // Disable on brillance all
        disa = 1;
        Device ds = DeviceFactory.getInstance().getDevice(mp.BPMALL_DISABLE[disa]);
        sendCommand(ds,devName,"DisableDevice");
      }
      
      if(type.equalsIgnoreCase("Spark")) {
        // Disable on spark all
        disa = 2;
        Device ds = DeviceFactory.getInstance().getDevice(mp.BPMALL_DISABLE[disa]);
        sendCommand(ds,devName,"DisableDevice");
      }
            
    } catch(DevFailed e1) {
      ErrorPane.showErrorMessage(null,mp.BPMALL_DISABLE[disa], e1);
    } catch(ConnectionException e2) {
      ErrorPane.showErrorMessage(null,mp.BPMALL_DISABLE[disa], e2);      
    }
    
    /*
    // Disable on orbit correction
    try {
    
      Device svdH = DeviceFactory.getInstance().getDevice("sr/beam-orbitcor/svd-h");
      Device svdV = DeviceFactory.getInstance().getDevice("sr/beam-orbitcor/svd-v");
      DeviceAttribute da = svdH.readAttribute("DisabledBPMs");
      String[] bpms = da.extractStringArray();
      // Search if bpm already disabled
      boolean found = false;
      int i=0;
      while(!found && i<bpms.length) {
        found = devName.equalsIgnoreCase(bpms[i]);
        if(!found) i++;
      }
      if(!found) {
        String[] nbpms = new String[bpms.length+1];
        for(i=0;i<bpms.length;i++)
          nbpms[i] = bpms[i];
        nbpms[i] = devName.toLowerCase();
        DeviceAttribute nda = new DeviceAttribute("DisabledBPMs");
        nda.insert(nbpms);
        svdH.writeAttribute(nda);
        svdV.writeAttribute(nda);
      }
      
    } catch(DevFailed e1) {
      ErrorPane.showErrorMessage(null,"sr/beam-orbitcor", e1);
    } catch(ConnectionException e2) {
      ErrorPane.showErrorMessage(null,"sr/beam-orbitcor", e2);      
    }
    */
    
  }

  public void launchPanel(String devName) {
      new atkpanel.MainPanel(devName, false, true, true);    
  }
  
}
