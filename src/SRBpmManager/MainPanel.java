/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRBpmManager;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributePolledList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.IStringSpectrum;
import fr.esrf.tangoatk.core.attribute.DevStateSpectrum;
import fr.esrf.tangoatk.core.attribute.StringSpectrum;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.attribute.SimpleStringSpectrumViewer;
import fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.Splash;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 *
 * @author pons
 */
public class MainPanel extends javax.swing.JFrame {
  
  private final static String APP_RELEASE = "3.13";
  final static int NBCELL = 32;
  final static String CLOCK_DEVNAME = "srdiag/bpm/aclock";
  final static String CLOCKSRC_DEVNAME = "sy/d-treff/switch";
  final static String CT_DEVNAME = "srdiag/beam-current/total";
  final static String DOOR_SERVER = "srdiag/door/bpm";
  
  String BPMALL_DEVNAME;
  int NBBPM;
  int TOTALBPM;
  int[] BPMIDX;
  String[] BPMTYPE;
  String[] BPMALL_DISABLE;
  String appTitle;
  AttributeList attList;
  CommandList cmdList;
  ErrorHistory errWin = null;
  String appName;
  Database db;
  
  Splash splash;
  private BPMStateViewer bpmStateViewer;
  private XZPositionPanel xzPosition;
  private SimpleStringSpectrumViewer errorViewer;
  
  private JFrame restoreUsmFrame = null;
  private SimpleStringSpectrumViewer doorOutputModel = new SimpleStringSpectrumViewer();

  private boolean runningFromShell;

  /**
   * Creates new form MainPanel
   */
  public MainPanel(boolean runningFromShell,String appName) {

    this.appName = appName;

    // Splash window
    splash = new Splash();
    splash.setTitle(appName + " " + APP_RELEASE);
    splash.setCopyright("(c) ESRF 2019");
    splash.setMaxProgress(100);
    splash.progress(0);
    
    // Get config
    try {
      db = ApiUtil.get_db_obj();
      BPMALL_DEVNAME = db.get_property(appName, "BPMALL").extractString();
      appTitle = db.get_property(appName, "Title").extractString();
      NBBPM = db.get_property(appName, "NB_BPM").extractLong();
      BPMIDX = db.get_property(appName, "BPMIDX").extractLongArray();
      BPMTYPE = db.get_property(appName, "BPMTYPE").extractStringArray();
      BPMALL_DISABLE = db.get_property(appName, "BPMALL_disable").extractStringArray();
      TOTALBPM = NBCELL*NBBPM;
    } catch( DevFailed e ) {
      splash.setVisible(false);
      ErrorPane.showErrorMessage(null, appName + " default configuration", e);
      System.exit(0);
    }
    splash.progress(10);

    this.runningFromShell = runningFromShell;
    initComponents();
    
    boolean isBpmAll = appName.equalsIgnoreCase("srbpm_all");
    
    jMenuItemRestoreUSM.setVisible(isBpmAll);
    jMenuCommands.setVisible(isBpmAll);
    
    // Handle windowClosing (Close from the system menu)
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitForm();
      }
    });
    
    if( errWin==null ) errWin = new ErrorHistory();
    attList = new AttributeList();
    attList.addErrorListener(errWin);
    attList.addErrorListener(ErrorPopup.getInstance());
    attList.addSetErrorListener(ErrorPopup.getInstance());

    cmdList = new CommandList();
    cmdList.addErrorListener(errWin);
    cmdList.addErrorListener(ErrorPopup.getInstance());
    
    ClockPanel cp = new ClockPanel(this);
    upPanel.add(cp,BorderLayout.NORTH);      

    bpmStateViewer = new BPMStateViewer(this);
    upPanel.add(bpmStateViewer,BorderLayout.CENTER);
    
    try {      
      DevStateSpectrum states = (DevStateSpectrum)attList.add(BPMALL_DEVNAME+"/states");
      bpmStateViewer.setModel(states);      
    } catch( ConnectionException e) {}
    splash.progress(20);
        
    GridBagConstraints gbc = new GridBagConstraints();
    JPanel set1Panel = new JPanel();
    set1Panel.setLayout(new GridBagLayout());    
    gbc.fill = GridBagConstraints.BOTH;

    try {
      
      DbDatum dd = db.get_property(appName, "PanelList");
      String[] panelList = dd.extractStringArray();
      for(int panel=0;panel<panelList.length;panel++) {
        
        String panelName = panelList[panel];
        String[] attList = db.get_property(appName, "Panel_" + panelName).extractStringArray();
        gbc.gridx = panel;
        gbc.gridy = 0;    
        SettingsPanel p = new SettingsPanel(this,panelName,attList);
        set1Panel.add(p,gbc);
        
      }
            
    } catch( DevFailed e ) {
      splash.setVisible(false);
      ErrorPane.showErrorMessage(null, appName + " Panel configuration", e);
      System.exit(-1);      
    }

    splash.progress(50);
    
    // Settings tab
    settingTabPane.add(set1Panel,"Settings");

    // Detailled settings tab
    settingTabPane.add(new AllSettings(this),"Detailed Settings");

    splash.progress(60);

    // HV position tab
    xzPosition = new XZPositionPanel(this);
    try {      
      INumberSpectrum x = (INumberSpectrum)attList.add(BPMALL_DEVNAME+"/All_SA_HPosition");
      INumberSpectrum z = (INumberSpectrum)attList.add(BPMALL_DEVNAME+"/All_SA_VPosition");
      xzPosition.setModel(x,z);
    } catch( ConnectionException e) {}
    settingTabPane.add(xzPosition,"HV Position");        
    
    
    /*
    if( appName.equalsIgnoreCase("SRBPM_All")) {
      TBTPanel tbpPanel = new TBTPanel(this);
      settingTabPane.add(tbpPanel,"TBT Image");
    }
    */
    
    splash.progress(70);
    
    // Spectrum panels
    try {
      
      DbDatum dd = db.get_property(appName, "SpectrumPanelList");
      String[] panelList = dd.extractStringArray();
      for(int panel=0;panel<panelList.length;panel++) {
        
        String panelName = panelList[panel];
        String[] attList = db.get_property(appName, "SpectrumPanel_" + panelName).extractStringArray();
        SpectrumPanel p = new SpectrumPanel(this,panelName,attList);
        settingTabPane.add(p,panelName);        
        
      }
            
    } catch( DevFailed e ) {
      splash.setVisible(false);
      ErrorPane.showErrorMessage(null, appName, e);
      System.exit(0);      
    }
    
    splash.progress(90);
       

    // Error viewers
    JPanel errorPanel = new JPanel();
    errorPanel.setLayout(new BorderLayout());
    JPanel btnPanel = new JPanel();
    btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    errorPanel.add(btnPanel,BorderLayout.SOUTH);
    VoidVoidCommandViewer resetErrors = new VoidVoidCommandViewer();
    btnPanel.add(resetErrors);
        
    errorViewer = new SimpleStringSpectrumViewer();
    errorPanel.add(errorViewer,BorderLayout.CENTER);
    settingTabPane.add("Errors",errorPanel);
    
    try {      
      StringSpectrum err = (StringSpectrum)attList.add(BPMALL_DEVNAME + "/Errors");
      errorViewer.setModel(err);      
      VoidVoidCommand resetErr = (VoidVoidCommand)cmdList.add(BPMALL_DEVNAME + "/ResetError");
      resetErrors.setModel(resetErr);
    } catch( ConnectionException e) {}
    
    try {
        AttributeList attListPolled = new AttributePolledList();
        doorOutputModel.setModel((IStringSpectrum)attListPolled.add(DOOR_SERVER + "/Output"));
        attListPolled.startRefresher();
    } catch( ConnectionException e) {}
    
    attList.setRefreshInterval(2000);
    attList.startRefresher();

    splash.progress(100);
    
    setTitle("BPMManager [" + appTitle + "] " + APP_RELEASE);
    ATKGraphicsUtils.centerFrameOnScreen(this);
    splash.setVisible(false);
    
  }
  
  /**
   * Exit the application
   */
  public void exitForm() {

    if (runningFromShell) {

      System.exit(0);

    } else {

      clearModel();
      bpmStateViewer.clearModel();
      setVisible(false);
      dispose();

    }

  }

  private void clearModel() {
    
    attList.removeErrorListener(errWin);
    attList.removeErrorListener(ErrorPopup.getInstance());
    attList.stopRefresher();
    attList.clear();

  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        upPanel = new javax.swing.JPanel();
        centerPanel = new javax.swing.JPanel();
        settingTabPane = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jFileMenu = new javax.swing.JMenu();
        jExitMenuItem = new javax.swing.JMenuItem();
        jViewMenu = new javax.swing.JMenu();
        errorMenuItem = new javax.swing.JMenuItem();
        diagMenuItem = new javax.swing.JMenuItem();
        jMenuCommands = new javax.swing.JMenu();
        jMenuItemRestoreUSM = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        upPanel.setLayout(new java.awt.BorderLayout());
        getContentPane().add(upPanel, java.awt.BorderLayout.NORTH);

        centerPanel.setLayout(new java.awt.BorderLayout());
        centerPanel.add(settingTabPane, java.awt.BorderLayout.CENTER);

        getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);

        jFileMenu.setText("File");

        jExitMenuItem.setText("Exit");
        jExitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jExitMenuItemActionPerformed(evt);
            }
        });
        jFileMenu.add(jExitMenuItem);

        jMenuBar1.add(jFileMenu);

        jViewMenu.setText("View");

        errorMenuItem.setText("Errors...");
        errorMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                errorMenuItemActionPerformed(evt);
            }
        });
        jViewMenu.add(errorMenuItem);

        diagMenuItem.setText("Diagnostics...");
        diagMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diagMenuItemActionPerformed(evt);
            }
        });
        jViewMenu.add(diagMenuItem);

        jMenuBar1.add(jViewMenu);

        jMenuCommands.setText("Commands");

        jMenuItemRestoreUSM.setText("Restore BPM Configuration for Usm");
        jMenuItemRestoreUSM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRestoreUSMActionPerformed(evt);
            }
        });
        jMenuCommands.add(jMenuItemRestoreUSM);

        jMenuBar1.add(jMenuCommands);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

  private void jExitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jExitMenuItemActionPerformed
    exitForm();
  }//GEN-LAST:event_jExitMenuItemActionPerformed

  private void errorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorMenuItemActionPerformed
    ATKGraphicsUtils.centerFrameOnScreen(errWin);
    errWin.setVisible(true);
  }//GEN-LAST:event_errorMenuItemActionPerformed

  private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
    fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
  }//GEN-LAST:event_diagMenuItemActionPerformed

    private void jMenuItemRestoreUSMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRestoreUSMActionPerformed
      try {
          Device door = ((Device)doorOutputModel.getModel().getDevice());
          DeviceData dd = new DeviceData();
          dd.insert(new String[]{"restore_usm"});
          door.command_inout("RunMacro",dd);
          
          if (restoreUsmFrame == null) {
              GridBagConstraints gbc = new GridBagConstraints();
              restoreUsmFrame = new JFrame();
              restoreUsmFrame.setTitle("Restore BPM Configuration for Usm");
              restoreUsmFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
              restoreUsmFrame.getContentPane().setLayout(new GridBagLayout());
              doorOutputModel.setBorder(new javax.swing.border.TitledBorder( DOOR_SERVER + "/Output"));
              gbc = new GridBagConstraints();
              gbc.fill = GridBagConstraints.BOTH;
              gbc.insets = new Insets(3, 3, 3, 3);
              gbc.gridx = 0;
              gbc.gridy = 0;
              gbc.weightx = 1.0;
              gbc.weighty = 1.0;
              restoreUsmFrame.getContentPane().add(doorOutputModel, gbc);
              gbc.anchor = GridBagConstraints.EAST;
              gbc.fill = GridBagConstraints.NONE;
              gbc.insets = new Insets(0, 0, 13, 15);
              gbc.gridy = 1;
              gbc.weighty = 0.0;
              JButton buttonEnd = new JButton("Close");
              buttonEnd.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                      restoreUsmFrame.setVisible(false);
                  }
              });
              restoreUsmFrame.getContentPane().add(buttonEnd, gbc);
              restoreUsmFrame.setPreferredSize(new Dimension(700, 400));
              restoreUsmFrame.pack();
              restoreUsmFrame.setLocationRelativeTo(this);
              restoreUsmFrame.setVisible(true);
              
          } else {
              restoreUsmFrame.setVisible(true);
          }
      } catch (DevFailed ex) {
          javax.swing.JOptionPane.showMessageDialog(
                    null, (String) ex.errors[0].desc, "ERREUR",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
          Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
      }
    }//GEN-LAST:event_jMenuItemRestoreUSMActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    if(args.length!=1) {
      System.out.println("Usage: jsrbm appname");
      System.out.println("appname: name of application, SRBPM_Spark , SRBPM_Brilliance , SRBPM_All");
      System.exit(0);
    }
    new MainPanel(true,args[0]).setVisible(true);
  }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel centerPanel;
    private javax.swing.JMenuItem diagMenuItem;
    private javax.swing.JMenuItem errorMenuItem;
    private javax.swing.JMenuItem jExitMenuItem;
    private javax.swing.JMenu jFileMenu;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuCommands;
    private javax.swing.JMenuItem jMenuItemRestoreUSM;
    private javax.swing.JMenu jViewMenu;
    private javax.swing.JTabbedPane settingTabPane;
    private javax.swing.JPanel upPanel;
    // End of variables declaration//GEN-END:variables
}
