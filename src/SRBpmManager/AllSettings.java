package SRBpmManager;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfoEx;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLAxis;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class AllSettings extends javax.swing.JPanel implements ActionListener,IJLChartListener {
  
  class Item {
    
    final static int INT_TYPE = 1;
    final static int BOOLEAN_TYPE = 2;
    final static int ENABLE_TYPE = 3;
    final static int DOUBLE_TYPE = 4;
    final static int ENUM_TYPE = 5;
    
    String name;
    int type;
    
    Item(MainPanel mp,String def) {
      
      String[] itemDef = def.split(",");
      if(itemDef.length!=2) {
        mp.splash.setVisible(false);
        JOptionPane.showMessageDialog(null, "Wrong DetailedSettings configuration (2 field expected): " + def, "Error", JOptionPane.ERROR_MESSAGE);
        System.exit(0);
      }

      this.name = itemDef[0];
      this.type = toType(mp,itemDef[1]);
    }
    
    private int toType(MainPanel mp,String typeStr) {
      
      if( typeStr.equalsIgnoreCase("INT_TYPE") ) {
        return INT_TYPE;
      } else if ( typeStr.equalsIgnoreCase("BOOLEAN_TYPE") ) {
        return BOOLEAN_TYPE;
      } else if ( typeStr.equalsIgnoreCase("ENABLE_TYPE") ) {
        return ENABLE_TYPE;
      } else if ( typeStr.equalsIgnoreCase("DOUBLE_TYPE") ) {
        return DOUBLE_TYPE;
      } else if ( typeStr.equalsIgnoreCase("ENUM_TYPE") ) {
        return ENUM_TYPE;
      } else {
        mp.splash.setVisible(false);
        JOptionPane.showMessageDialog(null, "Wrong DetailedSettings configuration (Unexpected type): " + typeStr, "Error", JOptionPane.ERROR_MESSAGE);
        System.exit(0);        
      }
      
      return 0;
      
    }
    
  }
  
  MainPanel mp;
  Item[] items; 
  DeviceProxy ds;
  JLDataView v;
  int selectedItem = -1;

  public AllSettings(MainPanel mp) {

    this.mp = mp;
    items = parseItems(mp.appName,mp.db);

    initComponents();
    
    if(!mp.appName.equalsIgnoreCase("SRBPM_Brilliance"))
        resetDaemonButton.setVisible(false);        

    for(int i=0;i<items.length;i++)
      nameCombo.addItem(items[i].name);
    nameCombo.setSelectedIndex(selectedItem);
    
    nameCombo.addActionListener(this);
    chart.setJLChartListener(this);
    
    try {
      ds = new DeviceProxy(mp.BPMALL_DEVNAME);
    } catch (DevFailed e) {
      ErrorPane.showErrorMessage(null, mp.BPMALL_DEVNAME, e);
    }
    
    v = new JLDataView();
    v.setMarker(JLDataView.MARKER_DOT);
    chart.getY1Axis().addDataView(v);
    chart.getY1Axis().setAutoScale(true);
    chart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);

    String[] cellNames = new String[MainPanel.NBCELL];
    double[] cellPos = new double[MainPanel.NBCELL];
    for(int i=0;i<cellNames.length;i++) {
      cellNames[i] = new String("C" + Integer.toString((i+3)%MainPanel.NBCELL+1));
      cellPos[i] = (double)i*mp.NBBPM;
    }
    chart.getXAxis().setLabels(cellNames, cellPos);
    chart.getXAxis().setGridVisible(true);
    chart.getXAxis().setAutoScale(true);
    
  }

  public void actionPerformed(ActionEvent e) {
    
    refreshSelected();
    
  }
  
  public void refreshSelected() {
    
    selectedItem = nameCombo.getSelectedIndex();
    if(selectedItem>=0) {
      double[] vals = read(items[selectedItem].name);
      v.setName(items[selectedItem].name);
      v.reset();
      for(int i=0;i<mp.TOTALBPM;i++)
        v.add(i,vals[i]);
      chart.repaint();
    }
    
  }
  
  public String[] clickOnChart(JLChartEvent jlce) {

    String[] ret = new String[2];
    
    int idx = (int)jlce.getXValue();
    int cellIdx = (idx/mp.NBBPM + 3)%mp.NBCELL + 1;
    int bpmIdx = mp.BPMIDX[idx%mp.NBBPM];
    
    ret[0] = "BPM C" + cellIdx + "-" + bpmIdx + " [" + mp.BPMTYPE[idx%mp.NBBPM] + "]";
    
    String name = items[selectedItem].name;
    if(name.startsWith("All_"))
      name = name.substring(4);
    double val = jlce.getYValue();
    
    switch(items[selectedItem].type) {
      case Item.INT_TYPE:
        ret[1] = name + "=" + Integer.toString((int)val);
        break;
      case Item.BOOLEAN_TYPE:
        ret[1] = name + "=" + Boolean.toString(val!=0.0);        
        break;
      case Item.ENABLE_TYPE:
        ret[1] = name + "=" + ((val!=0.0)?"Enabled":"Disabled");        
        break;
      case Item.ENUM_TYPE:

        // We need to get the string from the source device
        String bpmDevName = "srdiag/bpm/c" + String.format("%02d", cellIdx) + 
                            "-" + String.format("%02d", bpmIdx);
        try {
          
          DeviceProxy ds = new DeviceProxy(bpmDevName);
          AttributeInfoEx conf = ds.get_attribute_info_ex(name);
          int eVal = (int)(val);
          if(eVal>=0 && eVal<conf.enum_label.length) {
           ret[1] = name + "=" + conf.enum_label[eVal];                        
          } else {
           ret[1] = name + "=" + Integer.toString(eVal);            
          }
          
        } catch(DevFailed e) {
           ErrorPane.showErrorMessage(null, bpmDevName, e);                
        }
        
        break;
      default:
        ret[1] = name + "=" + Double.toString(val);
        break;
    }

    return ret;
    
  }
    
  private double[] read(String name) {

    double[] ret = new double[mp.TOTALBPM];
    for(int i=0;i<mp.TOTALBPM;i++) ret[i] = Double.NaN;
    
    try {    
      DeviceAttribute da = ds.read_attribute(name);
      double[] values = da.extractDoubleArray();
      for(int i=0;i<values.length && i<mp.TOTALBPM;i++) {
        ret[i] = values[i];
      }
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(null, mp.BPMALL_DEVNAME, e);      
    }
    
    return ret;
        
  }
  
  private Item[] parseItems(String appName,Database db) {
    
    Item[] ret = null;
    
    try {
      
      String[] itemList = db.get_property(appName, "DetailedSettings").extractStringArray();
      ret = new Item[itemList.length];
      for(int i=0;i<itemList.length;i++) {

        ret[i] = new Item(mp,itemList[i]);
                
      }
      
    } catch( DevFailed e) {
      mp.splash.setVisible(false);
      ErrorPane.showErrorMessage(null, appName + " DetailedSettings error", e);
      System.exit(0);
    }
   
    return ret;
    
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        upPanel = new javax.swing.JPanel();
        resetDaemonButton = new javax.swing.JButton();
        resetCounterButton = new javax.swing.JButton();
        refreshButton = new javax.swing.JButton();
        nameCombo = new javax.swing.JComboBox<>();
        chartPanel = new javax.swing.JPanel();
        chart = new fr.esrf.tangoatk.widget.util.chart.JLChart();

        setLayout(new java.awt.BorderLayout());

        upPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        resetDaemonButton.setText("Reset Events Daemon");
        resetDaemonButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetDaemonButtonActionPerformed(evt);
            }
        });
        upPanel.add(resetDaemonButton);

        resetCounterButton.setText("Reset TBT Counter");
        resetCounterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetCounterButtonActionPerformed(evt);
            }
        });
        upPanel.add(resetCounterButton);

        refreshButton.setText("Refresh");
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });
        upPanel.add(refreshButton);

        upPanel.add(nameCombo);

        add(upPanel, java.awt.BorderLayout.NORTH);

        chartPanel.setLayout(new java.awt.BorderLayout());
        chartPanel.add(chart, java.awt.BorderLayout.CENTER);

        add(chartPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

  private void resetCounterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetCounterButtonActionPerformed
        
    try {    
      ds.command_inout("ResetTriggerCounter");
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(null, mp.BPMALL_DEVNAME, e);      
    }    
    
  }//GEN-LAST:event_resetCounterButtonActionPerformed

  private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
    refreshSelected();
  }//GEN-LAST:event_refreshButtonActionPerformed

    private void resetDaemonButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetDaemonButtonActionPerformed
        // TODO add your handling code here:
        try {
            ds.command_inout("RestartEventDaemon");
        } catch (DevFailed e) {
            ErrorPane.showErrorMessage(null, mp.BPMALL_DEVNAME, e);
        }

    }//GEN-LAST:event_resetDaemonButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private fr.esrf.tangoatk.widget.util.chart.JLChart chart;
    private javax.swing.JPanel chartPanel;
    private javax.swing.JComboBox<String> nameCombo;
    private javax.swing.JButton refreshButton;
    private javax.swing.JButton resetCounterButton;
    private javax.swing.JButton resetDaemonButton;
    private javax.swing.JPanel upPanel;
    // End of variables declaration//GEN-END:variables


}
